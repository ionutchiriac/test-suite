package blog;
import Project.pages.BlogHomePage;
import Project.pages.LoginPage;
import Project.pages.Profile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class PageObjectFactoryTest extends BaseTestClass {

    private String userName = "ionutdaniel.chiriac@yahoo.com";
    private String password = "Chiriac123";
    String expectedMessage = "Profil mis à jour.";



    @Before
    public void setupImplicitWait() {
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
    }

    @Test
    public void pageObjectFactoryTest() {

        BlogHomePage blogPage = new BlogHomePage(driver);
        blogPage.navigateToLoginPage();

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);

        blogPage = loginPage.authentificate(userName, password);

        System.out.println(driver.findElement(By.linkText("Logout")));
        WebElement logout = driver.findElement(By.linkText("Logout"));
        Assert.assertEquals(logout.getText(), "Logout");
        System.out.println(driver.findElement(By.linkText("Howdy, ionutchiriac")));

        Profile newprofile = blogPage.navigateToProfile();

        Select anotherLanguage = new Select(driver.findElement(By.id("locale")));
        anotherLanguage.selectByValue("fr_FR");
        //*anotherLanguage.selectByIndex(7)
        newprofile.editProfile();
                   }

}
