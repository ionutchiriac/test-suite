package blog;

import org.apache.commons.lang3.SystemUtils;

import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTestClass {

    public WebDriver driver;
    String url = "http://practica.wantsome.ro/blog/contact/";
    private DriversPath driversPath = new DriversPath();

    @Before
    public void setUpTest() {
        System.setProperty("webdriver.chrome.driver", driversPath.getDriverDirPath() + "chromedriver" + driversPath.getDriverExtension());
        driver = new ChromeDriver();
        driver.get(url);
        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
    }

//    @After
//    public void tearDown() {
//        driver.quit();//    }
}