package Project.pages;

        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.support.FindBy;
        import org.openqa.selenium.support.PageFactory;

public class BlogHomePage {
    private WebDriver driver;

    @FindBy(xpath = "//ul[@id='menu-navigation']//a[contains(.,'Login')]")
    private WebElement loginLink;

    @FindBy(className = "display-name")
    private WebElement ProfileBtn;

    public BlogHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public LoginPage navigateToLoginPage() {
        loginLink.click();
        return new LoginPage(driver);
    }

    public Profile navigateToProfile() {
        ProfileBtn.click();
        return new Profile(driver);
    }
}