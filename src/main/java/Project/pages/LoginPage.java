package Project.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {
    private WebDriver driver;

    @FindBy(xpath = "//p[@class='login-username']/input[@name ='log']")
    private WebElement userName;

    @FindBy(xpath = "//label[@for='user_pass']/following-sibling::input[@id='user_pass']")
    private WebElement password;

    @FindBy(xpath = "//div[@id='wppb-login-wrap']//p[@class='login-submit']/input[@id='wppb-submit']")
    private WebElement loginBtn;

    @FindBy(linkText = "Logout")
    private WebElement logoutBtn;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }
    public BlogHomePage authentificate(String user, String pass) {
        userName.sendKeys(user);
        password.sendKeys(pass);
        loginBtn.click();
        logoutBtn.getText();
   return new BlogHomePage(driver);
    }


}
