package Project.pages;

import org.apache.bcel.generic.Select;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class Profile {
    private WebDriver driver;



       public Profile(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);

    }
    @FindBy(id = "admin_color_light")
    private WebElement color;

    @FindBy(id = "locale")
    private WebElement language;

    @FindBy(id = "submit")
    private WebElement submitProfile;

       public void editProfile(){
           color.click();
           submitProfile.click();


       }

          }


